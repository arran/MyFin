module.exports = {
    arrowParens: "avoid",
    endOfLine: "auto",
    tabWidth: 4,
    trailingComma: "all",
    quoteProps: "consistent",
    plugins: ["prettier-plugin-svelte"],
    svelteSortOrder: "options-styles-scripts-markup",
    svelteStrictMode: true,
    svelteBracketNewLine: true,
    svelteAllowShorthand: false,
    svelteIndentScriptAndStyle: false,
};
