import type { RxJsonSchema } from "rxdb";
import type { Member } from "../types/member.type";

export const MemberSchema: RxJsonSchema<Member> = {
    title: "Member",
    description: "A Member of the MyFin account",
    version: 0,
    type: "object",
    properties: {
        id: {
            type: "string",
            primary: true,
        },
        name: {
            type: "string",
        },
    },
    encrypted: ["name"],
    required: ["name"],
};
