export interface ITransaction {
    /**
     * Unique Identifier
     */
    readonly id: string;

    /**
     * UTC Date
     */
    date: string;

    /**
     * Display title
     */
    title: string;

    /**
     * Currency String for formatting (EUR, USD)
     */
    currency: string;

    /**
     * Array of MemberTransaction Credits
     */
    credits: Array<IMemberTransaction>;

    /**
     * Array of MemberTransaction Debits
     */
    debits: Array<IMemberTransaction>;
}

export interface IMemberTransaction {
    /**
     * MemberId of the transaction
     */
    id: string;

    /**
     * Amount in the lowest denonimation, usually cents.
     */
    amount: number;
}
