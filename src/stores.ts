import { Writable, writable } from "svelte/store";
import type { IToast, IToastStore } from "./types/store.types";

const createToastStore = () => {
    const { subscribe, set, update } = writable<IToast>({});

    return {
        subscribe,
        set,
        update,
        show: (text: string, style?: string) => {
            update((x: IToast) => (x = { content: text, style: style || "" }));
        },
        reset: () => set({}),
    };
};

export const toast: IToastStore = createToastStore();
export const currentMember: Writable<string> = writable("");
