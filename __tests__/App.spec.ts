import { render, cleanup } from "@testing-library/svelte";
import App from "../src/App.svelte";

describe("App", () => {
    afterEach(() => {
        cleanup();
    });
    it("should render", () => {
        const { container } = render(App);
        expect(container).toMatchSnapshot();
    });
});
