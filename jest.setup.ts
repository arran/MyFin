import "@testing-library/jest-dom";

// Mock fetch for now since RxDB somehow relies on it and breaks the tests since it is
// not defined in a node environment in which the tests run. Needs further
// investigation how to handle RxDB properly.
global.fetch = jest.fn();

// We need to import the rxdb bundle which assigns the RxDB namespace to the global
// object which is the same like the window object in the tests. That way we can inject
// the in-memory adapter for use in the tests without changing the current
// implementation.
import "rxdb/dist/rxdb.browserify.js";
import * as inMemoryAdapter from "pouchdb-adapter-memory";

declare const global: any;
global.RxDB.addRxPlugin(inMemoryAdapter);

import rxdbSettings from "./src/abstractions/db/rxdb.settings";
rxdbSettings.adapter = "memory";
